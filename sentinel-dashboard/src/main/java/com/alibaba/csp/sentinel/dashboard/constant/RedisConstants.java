package com.alibaba.csp.sentinel.dashboard.constant;

public class RedisConstants {
	/**流控规则发布通道*/
	public static final  String SENTILE_FLOW_RULE_CHANNEL = "sentileFlowruleChannel";
	/**流控规则key*/
	public static final  String SENTILE_RULE_FLOW = "sentile:rule:flow";

}
