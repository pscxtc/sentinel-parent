package com.alibaba.csp.sentinel.demo.spring.sc.gateway.config;

import com.alibaba.csp.sentinel.adapter.gateway.common.rule.GatewayFlowRule;
import com.alibaba.csp.sentinel.adapter.gateway.common.rule.GatewayRuleManager;
import com.alibaba.csp.sentinel.adapter.gateway.sc.callback.BlockRequestHandler;
import com.alibaba.csp.sentinel.adapter.gateway.sc.callback.GatewayCallbackManager;
import com.alibaba.csp.sentinel.datasource.ReadableDataSource;
import com.alibaba.csp.sentinel.datasource.nacos.NacosDataSource;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.TypeReference;
import com.alibaba.nacos.api.PropertyKeyConst;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.server.ServerResponse;

import javax.annotation.PostConstruct;
import java.util.Properties;
import java.util.Set;

/**
 * nacos 配置类
 *
 * @author chenxu
 * @since 2021年01月14日
 */
@Configuration
public class GatewayConfiguration {

    @Value("${spring.cloud.sentinel.datasource.ds.nacos.server-addr}")
    private String NACOSCONFIG_SERVERADDR;
    @Value("${spring.cloud.sentinel.datasource.ds.nacos.namespace}")
    private String NACOSCONFIG_NAMESPACE;
    @Value("${spring.cloud.sentinel.datasource.ds.nacos.groupId}")
    private String NACOSCONFIG_GROUPID;
    @Value("${spring.cloud.sentinel.datasource.ds.nacos.dataId}")
    private String NACOSCONFIG_FILENAME;

    private static JSONObject blockReturn = new JSONObject();
    static {
        blockReturn.put("code","E00501");
        blockReturn.put("msg","已被限流");
    }

    /**
     * nacos 配置加载
     * @since 2021/1/18
     * 版本历史:
     * Date         Author         Description
     *---------------------------------------------------------*
     * 2021/1/18   chenxu          初始创建
     */
    @Bean
    public void initNacos(){
        Properties properties = new Properties();
        properties.put(PropertyKeyConst.SERVER_ADDR, NACOSCONFIG_SERVERADDR);
        properties.put(PropertyKeyConst.NAMESPACE, NACOSCONFIG_NAMESPACE);

        ReadableDataSource<String, Set<GatewayFlowRule>> flowRuleDataSource = new NacosDataSource<>(properties, NACOSCONFIG_GROUPID, NACOSCONFIG_FILENAME,
                source -> JSON.parseObject(source, new TypeReference<Set<GatewayFlowRule>>() {}));
        GatewayRuleManager.register2Property(flowRuleDataSource.getProperty());
    }


    /**
     * 自定义限流信息
     * @since 2021/1/18
     * 版本历史:
     * Date         Author         Description
     *---------------------------------------------------------*
     * 2021/1/18   chenxu          初始创建
     */
    @PostConstruct
    public void initBlockHandler(){
        BlockRequestHandler blockRequestHandler = (serverWebExchange, throwable) -> ServerResponse.status(HttpStatus.INTERNAL_SERVER_ERROR).contentType(MediaType.APPLICATION_JSON).body(BodyInserters.fromObject(blockReturn) );
        GatewayCallbackManager.setBlockHandler(blockRequestHandler);
    }
}
